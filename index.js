const scrapeIt = require('scrape-it');

const months = {
  Jan: 0,
  Feb: 1,
  Mar: 2,
  Apr: 3,
  May: 4,
  Jun: 5,
  Jul: 6,
  Aug: 7,
  Sep: 8,
  Oct: 9,
  Nov: 10,
  Dec: 11,
}
const monthmatcher = new RegExp(`(${Object.keys(months).join('|')}). (\\d+)`);
const year = 2020;

const timelinelabels = {
  'Voting early': 'Early voting begins',
  'Registration': 'Must be registered to vote',
  'Requesting an absentee ballot': 'Request an absentee ballot',
  'Submitting an absentee ballot': 'Submit an absentee ballot',
  'In-person voting': 'In-person voting begins',
}

scrapeIt('https://projects.fivethirtyeight.com/how-to-vote-2020/', {
  states: {
    listItem: '.state-wrapper',
    data: {
      name: '.state-name',
      info: {
        listItem: '.info-box',
        data: {
          header: '.info-hed',
          text: {
            selector: '.info-text',
            how: 'html',
          },
          date: {
            selector: '.info-text',
            convert: (text) => {
              const result = text.match(monthmatcher);
              // console.log(result, text)
              if (result) return new Date(Date.UTC(year, months[result[1]], result[2]));
            }
          }
          // linktext: 'a',
          // link: {
          //   selector: 'a',
          //   attr: "href"
          // }
        },
      },
    },
  },
}).then(({ data, response }) => {
  // console.log(`Status Code: ${response.statusCode}`)

  // console.log('data', data)

  const result = {};
  data.states.map(state => {
    const info = {};
    const timeline = [];
    state.info.map((data) => {
      info[data.header] = {text: data.text};
      if (data.date) {
        const timelinelabel = timelinelabels[data.header]
        if (timelinelabel) {
          timeline.push({
            date: data.date,
            text: timelinelabel,
          });
        }
        info[data.header].date = data.date;
      }
      // console.log('data', data)
    })
    timeline.sort((a, b) => {
      return a.date - b.date;
    })
    info.timelineData = timeline;
    // console.log(state.name, JSON.stringify(info));
    result[state.name] = info;
  });
  console.log(JSON.stringify(result));
});
